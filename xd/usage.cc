//                     usage.cc

#include "main.ih"

namespace
{

char const info1[] = 
R"( [options] args
Where:
   [options] - optional arguments (short options and default values between
               parentheses):
  --all (-a)                - skip `ignore' specification in the
                              configuration file
  --block-size (-b) size    - show the alternatives in blocks of max.
                              'size' alternatives (spacebar to continue)
  --config-file (-c) <file> - path to the config file to use
                              ($HOME/.xdrc)
  --add-root <when>         - search expansions from / (if-empty)
  --directories <which>     - which directories to show? (default: all)
  --generalized-search (-g) - use the GDS mode
  --help (-h)               - provide this help
  --history <fname>         - use <fname> to store info about choices
                              (no history unless specified)
  --history-lifetime <spec> - specify the max. lifetime of previously made
                              choices. Use <int>[DWMY] for a lifetime of
                              Days, Months, Weeks, or Years
  --history-position <where = TOP>  - where to put the previously made choices
                              (TOP, BOTTOM)
  --history-maxsize <size = UINT_MAX>  - display at most <size> previously
                              made choices
  --history-separate        - separate previously made choices from new ones
                              by a blank line (not with --block-size)
  --input                   - a non-selection character ends XD and is entered
                              into the shell's input stream
  --start-at <origin>       - where to start the search? (default: home dir.)
  --traditional             - use the traditional mode
  --version (-v)            - show version information and terminate
  --verbose (-V)            - show )";

char const info2[] = 
R"('s actions in detail
  args     - arguments, possibly containing directory separators [/-].

)";

char const info3[] = 
R"( eXchanges Directories by interpreting the characters of its
argument(s) as the initial characters of nested subdirectories.
Multiple arguments or arguments separated by / or - define the
initial characters of subsequently nested subdirectories.

If the first argument starts with . expansion starts at the user's
home directory; if it's 0 expansion starts in the current directory;
if it's / expansion starts at the root; if it's a number (1 .. 9) 
expansion starts at parent <number>; otherwise expansion starts
at the location defined by the configuration file

When the specification results in multiple solutions, a final
selection is requested from a displayed list of alternatives.

Use 'man xd' or read the xdrc file provided with the distribution
for details about ")";

char const info4[] =
R"('s configuration file

)";

}

void usage(std::string  const  &progname)
{
    cerr << "\n" <<
    progname << " by " << Icmbuild::author << "\n" <<
    progname << " V" << Icmbuild::version << " " << Icmbuild::years << "\n"
    "\n"
    "Usage: " << progname << info1 << progname << info2 <<
    progname << info3 <<  progname << info4;
}






