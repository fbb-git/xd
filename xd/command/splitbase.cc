#include "command.ih"

void Command::splitBase()
{
    if (size() != 1)
        return;

    for_each(                   // copy all spec. elements into Command's 
        front().begin() + 1, front().end(),                 // base object
        [&](char ch)
        {
            push_back(string(1, ch));
        }
    );

    front().resize(1);          // keep the 1st character
}
