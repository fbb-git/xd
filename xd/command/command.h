#ifndef _INCLUDED_COMMAND_H_
#define _INCLUDED_COMMAND_H_

#include <string>
#include <vector>

// determine the command as received and the kind of action according to
// the received pattern.

// the pattern is converted to its elements and each element is stored in
// an element of the vector base class. E.g., xd 2abc stores a, b, c
// and 'xd 3 ab cd ef stores ab, bc and ef.

// The initial location character is used to determine the Action. E.g.,
// 0: FROM_CWD, /: FROM_ROOT, 1..9: FROM_PARENT, .: FROM_HOME

// d_arguments contains the individual args, ending in /.
// E.g., 2abc -> abc/, 2 ab cd de -> ab/cd/de/

struct Command: public std::vector<std::string>
                       // stores the elements of the pattern
{
    // modify commanddata.cc if Action is modified
    enum Action                     // starting point as determined
    {                               // by the first arg-character
        FROM_CONFIG,                // default: determined by config
        FROM_HOME,
        FROM_ROOT,
        FROM_CWD,
        FROM_PARENT,                // relative to CWD
    };

    private:
        Action d_action;
        size_t d_parent;

        int d_homedirChar = '.';            // default homedir char
                                            // the selection args, each ending
        std::string d_arguments;            // in / 
    
        static char const *s_action[];
        static char const s_separators[];   // separating parts of nested dir
                                            // names
    public:
        Command();

        std::string const &accumulate() const;  // d_arguments
        size_t parent() const;                  // FROM_PARENT number
        Action action() const;                  // Action type

    private:
        void concatArgs();
        void determineAction();
        void splitBase();                   // split [0]'s characters into
                                            // separate vector elements
};

inline size_t Command::parent() const
{
    return d_parent;
}

inline Command::Action Command::action() const
{
    return d_action;
}

inline std::string const &Command::accumulate() const
{
    return d_arguments;
}


#endif
