#include "alternatives.ih"

void Alternatives::inspect(char const *entry)
{
    imsg << "Inspecting `" << entry << "': ";

                            
    string dirEntry(entry);     // if a trailing / was removed reinstall it.
    if (dirEntry.back() != '/')
        dirEntry += '/';

                                // no patterns with /./ or /. elements
    if (embeddedDot(dirEntry) or trailingDots(entry))
        return;

    Stat stat{ entry };

                                // if 'all dirs' is not specified then 
                                // entry must be equal to the true path name
    if (not d_allDirs && stat.path() != entry)
    {
        imsg << "   symlink" << endl;
        return;
    }
                                // only add unique entries 
    if (d_accepted.insert( { stat.inode(), stat.device() } ).second == false)
    {
        imsg << "   already available" << endl;
        return;
    }

    imsg << "ACCEPTED" << endl;

    add(entry);             // store the entry in the deque 'parent' class
}
