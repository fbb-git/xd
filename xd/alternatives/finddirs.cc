#include "alternatives.ih"

void Alternatives::findDirs()
{
    bool generalized = 
            d_options.generalized() and not d_options.traditional();

    Result (Alternatives::*find)(string const &dir) =
            (
                generalized ?
                    &Alternatives::generalizedDirs
                :
                    &Alternatives::traditionalDirs
            );

        // first try to find alternatives from the initial dir.
    d_result = (this->*find)(d_initialDir);

    imsg << "# matches using " << 
            (generalized ? "generalized" : "traditional") << " searching: " <<
             size() << endl;

        // if that doesn't work, and searching from / is OK (IF_EMPTY)
        // or searching fm / should always be performed then also search
        // fm /. No need to do that if initialDir == '/'
    if (
        d_initialDir != "/" 
        and 
        (
            d_addRoot == ALWAYS 
            or 
            (size() == 0 && d_addRoot == IF_EMPTY)
        )
    )
    {
        imsg << "Searching again from the root-dir" << endl;
        d_result = (this->*find)("/");
    }

//    if (d_result == NONE)
//    {
//        imsg << "No matches..." << endl;
//        throw NONE;
//    }

}



