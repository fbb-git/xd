#include "alternatives.ih"

// abcd is handled as:
//
// 1. a*/       if existing: test bcd else leave.
//
// 1.1. a*/b*   if existing: test cd else leave (etc).
//
// 2.  ab*/ test cd (etc).
//
// but if a/bcd is entered:
//
// 1. a*/       if existing: test /bcd, else leave
//
// 1.1 a*/b*    if existing: test cd, else leave (etc)
//
// 2.  a/b:    not tested
// 3.  a/bc:   not tested
// 4.  a/bcd:  not tested
//
// So: if the head contains a / it is not tested.
// If the tail starts with /, that char is ignored.

    // add alternatives found with generalized directory searching
    // dir: starting directory
void Alternatives::addAlternatives(size_t *idx, string &searchCmd, string dir)
try
{
    prepareDir(dir, idx, searchCmd);        // prepare dir for globbing

    globAlternatives(*idx, searchCmd, dir);
}
catch (exception const &err)        // exception by Glob
{
    imsg << "No pattern matching `" << dir << "', pruning this branch" <<
            endl;

    throw false;
}

