#include "alternatives.ih"

void Alternatives::globAlternatives(size_t idx, string const &searchCmd,
                              string const &dir)
{
    Glob glob(Glob::DIRECTORY, dir, Glob::NOSORT, Glob::DEFAULT);
    imsg << "Pattern `" << dir << "', " << glob.size() <<
            " matches" << endl;

    if (idx != searchCmd.length())
    {
        string tail = searchCmd.substr(idx);
        if (tail[0] == '/')
            tail.erase(0,1);
        generalizedAlternatives(dir, tail);
        return;
    }

    for (auto entry: glob)
        inspect(entry);
}
