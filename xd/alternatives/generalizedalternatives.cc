#include "alternatives.ih"

void Alternatives::generalizedAlternatives(string const &initial, 
                                           string &searchCmd)
try
{
    size_t idx = 0;
    size_t end = searchCmd.length();

    while (idx != end)
        addAlternatives(&idx, searchCmd, initial);
}
catch (bool headHasSlash)
{}
