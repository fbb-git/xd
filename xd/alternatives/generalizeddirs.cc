#include "alternatives.ih"

Result Alternatives::generalizedDirs(string const &initial)
{
        // create the command consisting of all cmd line args ending in /
        // E.g., 2abc -> abc/, 2 ab cd de -> ab/cd/de/
    string searchCmd = d_command.accumulate();

    Result ret = searchCmd.length() == 0 ? DIRECT : MULTIPLE;

    if (ret == MULTIPLE)
        searchCmd.pop_back();                   // remove trailing /

    if (searchCmd.empty())
        imsg << "Direct CD, no merged searching, cd to ";
    else
        imsg << "Merged searching: `" << searchCmd << "', searching from ";

    imsg << initial << '\'' << endl;

    if (ret == MULTIPLE)
        generalizedAlternatives(initial, searchCmd);

    return ret;
}
