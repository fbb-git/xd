#include "alternatives.ih"

void Alternatives::order()
{
    if (d_history.rotate())
        rotate(begin(), begin() + d_nInHistory, end());
}
