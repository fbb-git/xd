#include "alternatives.ih"

string const &Alternatives::operator[](size_t index) const
{
    return d_result == DIRECT ?
                d_initialDir
            :
                deque<string>::operator[](index);
}
