#include "history.ih"

    // choice: chosen directory to cd to

void History::save(string const &choice)
{
    if (d_historyFilename.empty())               // no history file in use
        return;

    ofstream out(d_historyFilename);
    if (!out)
    {
        imsg << "cannot write history file `" << d_historyFilename << '\'' << 
                                                                        endl;
        return;
    }

    auto iter = findIter(choice);

    if (iter == d_history.end())
        d_history.push_back(HistoryInfo(d_now, 1, choice));
    else
    {
        HistoryInfo *info = const_cast<HistoryInfo *>(&*iter);
        ++info->count;
        info->time = d_now;
    }

    sort(d_history.begin(), d_history.end(), compareTimes);
//    stable_sort(d_history.begin(), d_history.end(), compareCounts);

    if (size_t maxSize = d_options.historyMaxSize(); maxSize != UINT_MAX)
    {
        imsg << "Max. history size: " << maxSize << endl;

        if (d_history.size() > maxSize)
            d_history.resize(maxSize);
    }

    copy(d_history.begin(), d_history.end(),
                                ostream_iterator<HistoryInfo>(out, "\n"));
}

