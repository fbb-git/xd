#include "history.ih"

History::History()
:
    d_options(Options::instance()),
    d_now(d_options.now())
{
    imsg << "\n"
            "History\n";

    if (not d_options.history().empty())
    {
        d_historyFilename = d_options.homeDir() + d_options.history();
        load();
    }
}
