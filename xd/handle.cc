#include "main.ih"

int handle(exception_ptr ptr)
try
{
    rethrow_exception(ptr);
}
catch(exception const &err)     // handle exceptions
{
    cerr << err.what() << '\n';

                                            // preventa a directory change 
    noInput();                              // if --input wasn't specified

    return Selector::ERROR;
}
catch(int value)
{
    if (ArgConfig::instance().option("hv"))
        value = Selector::USAGE;
    else if (value == Result::NONE)
        cerr << "No Solutions\n";

    noInput();

    return value;
}





