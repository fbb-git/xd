#include "main.ih"

void noInput()
{
    if (not Options::input())
        cout << ".\n";                      // prevents a directory change 
}
