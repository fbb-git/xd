#include <iostream>
#include <vector>

using namespace std;


enum
{
    MIN_BLOCK_SIZE = 5
};

struct BlockStruct
{
    size_t begin;
    size_t end;
    char const *prompt;
};


size_t d_blockSize;

void set(BlockStruct &dest, size_t end)
{
    dest.begin = end;
    dest.end = end + d_blockSize;
}

void set(BlockStruct &preLast, BlockStruct &last)
{
    if (last.end - last.begin >= MIN_BLOCK_SIZE)    // last block: big enough
        return;

    size_t preSize = (last.end - preLast.begin + 1) / 2;

    preLast.end = preLast.begin + preSize;
    last.begin = preLast.end;
}

void alternativeBlocks(size_t nAlternatives)
{
                                                // # used blocks 
    size_t nBlocks = (nAlternatives + d_blockSize - 1) / d_blockSize;

    if (nBlocks == 1)
    {
        cout << "only 1 block\n";
        return;
    }

    vector<BlockStruct> blocks(nBlocks, {0, 0, "-+" });

    blocks.front().prompt = "+";
    blocks.front().end = d_blockSize;

    for (size_t idx = 1, end = blocks.size();  idx != end; ++idx)
        set(blocks[idx], blocks[idx - 1].end);

    blocks.back().prompt = "-";

    if (blocks.back().end > nAlternatives)      // back may not exceed 
        blocks.back().end = nAlternatives;      // nAlternatives

    set(*(blocks.rbegin() + 1), blocks.back()); // inspect the last two sizes


    for (auto const &block: blocks)
        cout << block.begin << " to " << block.end << 
                " (" << (block.end - block.begin) << ")"
                " with " << block.prompt << '\n';
    
}


int main(int argc, char **argv)
{
    while (true)
    {
        cout << "nAlternatives blockSize: ";
        size_t nAlt;
        cin >> nAlt >> d_blockSize;

        alternativeBlocks(nAlt);
    }
}
