#include "options.ih"

void Options::setPosition()
{
    string value;

    d_historyPosition =
        d_arg.option(&value, "history-position") && value == "bottom" ?
            BOTTOM
        :
            TOP;

    imsg << "History elements at the " <<
                (d_historyPosition == TOP ? "top" : "bottom") << endl;

}
