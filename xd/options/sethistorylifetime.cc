#include "options.ih"

void Options::setHistoryLifetime()
{
    string value;

    if (not d_arg.option(&value, "history-lifetime"))
    {
        d_historyLifetime = d_now - 24 * 60 * 60 * 30;   // 1 month lifetime
        imsg << "History lifetime: 1M" << endl;
        return;
    }

    try
    {
        d_historyLifetime = stoul(value);
    }
    catch (...)
    {
        imsg << "Cannot determine history-lifetime " << value <<
                ", using 1M" << endl;
    
        d_historyLifetime = d_now - 24 * 60 * 60 * 30;   // 1 month lifetime
        return;
    }

    int lastChar = toupper(*value.rbegin());

    imsg << "History lifetime: " << d_historyLifetime <<
                                    static_cast<char>(lastChar) << endl;

    d_historyLifetime = d_now - d_historyLifetime * 24 * 60 * 60 *
        (
            lastChar == 'W' ?   7 :
            lastChar == 'M' ?  30 :
            lastChar == 'Y' ? 365 : 1
        );
}
