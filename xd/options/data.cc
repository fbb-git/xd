#include "options.ih"

unique_ptr<Options> Options::s_options;

bool Options::s_input;

char Options::s_defaultConfig[] = ".xdrc";     // in $HOME
char Options::s_defaultHistory[] = ".xd.his";  // in $HOME

bool Options::s_fromHome[] =                    // values correspond to 
{                                               // s_startAt entries
    true,
    false,
    true
};

string const Options::s_startAt[] =
{
    "home",
    "root",
    ""
};
                        // the last element is used for the default value if
                        // valid alternatives aren't used
string const *Options::s_endStartAt = s_startAt + size(s_startAt) - 1;

bool Options::s_allDirs[] =
{
    true,
    false,
    true
};

string const Options::s_dirs[] =
{
    "all", 
    "unique",
    ""
};

string const *Options::s_endDirs = s_dirs + size(s_dirs) - 1;

TriState Options::s_addRoot[] =
{
    NEVER,
    IF_EMPTY,
    ALWAYS,
    IF_EMPTY
};

string const Options::s_triState[] =
{
    "never",
    "if-empty",
    "always",
    ""
};

string const *Options::s_endTriState = s_triState + size(s_triState) - 1;





