#include "selector.ih"

void Selector::showAlternatives()
{
                                                            // # used blocks 
    size_t nBlocks = (d_index + d_blockSize - 1) / d_blockSize;

    if (nBlocks == 1)                           // one block:
    {
        Block block{ 0, d_index, "" };
        show(block);                            // show the alternatives
        action(block);                          // and make a selection
        return;
    }

    vector<Block> blocks = blockVector(nBlocks);

    size_t idx = 0;
    while (true)
    {
        show(blocks[idx]);
        switch (action(blocks[idx]))
        {
            case Action::DEC:
                --idx;
            break;

            case Action::INC:
                ++idx;
            break;

            case Action::INPUT:
            return;
        }
    }
}



