#include "selector.ih"

    // alternatives is  +.> or -,<
    // if ch in alternatives, use alternatives.front() for accepted:
    // accepted is      +  -  or -+
    // 
    
// static
bool Selector::accept(int ch, string const &alternatives, 
                              string const &accepted) 
{
    return alternatives.find(ch) != string::npos    // +.>  -,<  or both
           and 
           accepted.find(alternatives.front()) != string::npos;
}
