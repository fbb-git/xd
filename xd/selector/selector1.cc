#include "selector.ih"

Selector::Selector(Alternatives &alternatives)
:
    d_alternatives(alternatives),
    d_index(d_alternatives.size()),
    d_ioctl(Options::instance().input()),
    d_direct(d_alternatives.result() == DIRECT),
    d_returnValue(ERROR)
{
    imsg << "\n"
            "Selector" << endl;

    setBlockSize();                     // # alternatives shown in a block
}
