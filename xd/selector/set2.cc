#include "selector.ih"

void Selector::set(Block &beforeLast, Block &last)
{
    if (last.end - last.begin >= MIN_BLOCK_SIZE)    // last block: big enough
        return;

    size_t beforeSize = (last.end - beforeLast.begin + 1) / 2;

    beforeLast.end = beforeLast.begin + beforeSize;
    last.begin = beforeLast.end;
}
