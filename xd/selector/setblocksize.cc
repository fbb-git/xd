#include "selector.ih"

void Selector::setBlockSize()
{
                                    // blockSize cannot exceed the #available
                                    // selection chars or the #available 
                                    // alternatives
    d_blockSize = Options::instance().blockSize();

    if (d_blockSize  < MIN_BLOCK_SIZE)
        d_blockSize = MIN_BLOCK_SIZE;
    else if (d_blockSize > d_index)
        d_blockSize = d_index <=  s_allChars.size() ? d_index
                                                    : s_allChars.size();
}

